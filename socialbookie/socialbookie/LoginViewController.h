//
//  LoginViewController.h
//  socialbookie
//
//  Created by David Hao on 2/10/14.
//  Copyright (c) 2014 David Hao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UIWebViewDelegate>

@end
