//
//  MoneyRankingViewController.m
//  
//
//  Created by David Hao on 8/12/14.
//
//

#import "MoneyRankingViewController.h"
#import "BackendCaller.h"
#import "UIImageView+AFNetworking.h"

@interface MoneyRankingViewController ()
    @property NSArray *feed;
    @property UIRefreshControl *refreshControl;
@end

@implementation MoneyRankingViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startRefresh)
                  forControlEvents:UIControlEventValueChanged];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"moneyRankingReceived" object:nil];
    [self.tableView addSubview:self.refreshControl];
    if (!self.feed)
    {
        [self startRefresh];
    }
}

- (void) startRefresh
{
    [BackendCaller getMoneyRanking];
}

- (void) refresh
{
    self.feed = [BackendCaller retrieveMoneyRanking];
    NSLog(@"%@", self.feed);
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSLog(@"%lu", (unsigned long)[self.feed count]);
    return [self.feed count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%@", indexPath);
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MoneyCell" forIndexPath:indexPath];
    
    NSDictionary *friend = (id)[self.feed objectAtIndex:indexPath.row];
    // Configure the cell...
    UIImageView *profileView = (id)[cell.contentView viewWithTag:100];
    [profileView setImageWithURL:[NSURL URLWithString:(NSString *)friend[@"profile_picture"]]];
    UILabel *rankLabel = (id)[cell.contentView viewWithTag:200];
    [rankLabel setText:[NSString stringWithFormat:@"%ld", indexPath.row + 1]];
    
    UILabel *moneyLabel = (id)[cell.contentView viewWithTag:300];
    [moneyLabel setText:[NSString stringWithFormat:@"$%@", friend[@"money"]]];
    
    UILabel *nameLabel = (id)[cell.contentView viewWithTag:400];
    [nameLabel setText:friend[@"username"]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
@end
