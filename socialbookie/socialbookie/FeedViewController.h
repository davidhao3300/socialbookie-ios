//
//  FeedViewController.h
//  
//
//  Created by David Hao on 2/10/14.
//
//

#import <UIKit/UIKit.h>

@interface FeedViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>

@end
