//
//  BetFeedViewController.m
//  socialbookie
//
//  Created by David Hao on 2/21/14.
//  Copyright (c) 2014 David Hao. All rights reserved.
//

#import "BetFeedViewController.h"
#import "BackendCaller.h"
#import "UIImageView+AFNetworking.h"

@interface BetFeedViewController ()
@property NSArray *feed;
@property UIRefreshControl *refreshControl;
@end

@implementation BetFeedViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeTimes) userInfo:nil repeats:YES];
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startRefresh)
                  forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
}

-(void) startRefresh
{
    NSLog(@"Starting refresh");
    [BackendCaller getBetFeed];
}
-(void) changeTimes
{
    NSArray *cells = [self.collectionView visibleCells];
    for (UICollectionViewCell *cell in cells)
    {
        UILabel *timeLabel = (UILabel *)[cell viewWithTag:102];
        NSDictionary *bet = (NSDictionary *) [self.feed objectAtIndex:[self.collectionView indexPathForCell:cell].row];
        long currentTime = [[NSDate date] timeIntervalSince1970];
        long expTime = [bet[@"end_time"] longValue];
        
        long difference = expTime - currentTime;
        
        if(difference > 0) timeLabel.text = [NSString stringWithFormat:@"%2ldh%2ldm%2lds", difference/3600, difference%3600/60, difference%60];
        else
        {
            if (bet[@"final_likes"] != nil)
            {
                
                timeLabel.text = [NSString stringWithFormat:@"%@", bet[@"payout"]];
            }
            else
            {
                [BackendCaller getStatus];
                break;
            }
        }
    }
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(refresh) name:@"betFeedReceived" object:nil];
    [self startRefresh];
}
-(void) refresh
{
    self.feed = [BackendCaller retrieveBetFeedData];
    NSLog(@"%@", self.feed);
    
    NSLog(@"Refresh completed");
    [self.collectionView reloadData];
    [self.refreshControl endRefreshing];
}
-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.feed count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *bet = [self.feed objectAtIndex:indexPath.row];
    static NSString *identifier = @"Cell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *photoImageView = (UIImageView *)[cell viewWithTag:100];
    if (bet[@"picture"])
    {
        NSURL *url = [NSURL URLWithString: (NSString *)(bet[@"picture"][@"small_link"])];
        [photoImageView setImageWithURL:url];
    }
    
    long currentTime = [[NSDate date] timeIntervalSince1970];
    long expTime = [bet[@"end_time"] longValue];
    
    long difference = expTime - currentTime;
    
    UIView *detailView = [cell viewWithTag:200];
    
    if(difference > 0)
    {
        UIImageView *resultView = (UIImageView *)[cell viewWithTag:300];
        resultView.image = nil;
        if ([[detailView.layer sublayers] count] > 0)
        {
            [[[detailView.layer sublayers] objectAtIndex:0] removeFromSuperlayer];
        }
        float pi = 3.1415;
        float endAngle = -0.5 * pi;
        float startAngle = 1.0 * difference / 60 / 60 * pi * 2/24 - 0.5*pi;
        UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(80, 80) radius:20 startAngle:startAngle endAngle:endAngle clockwise:false];
        [path addLineToPoint:CGPointMake(80, 80)];
        
        CAShapeLayer *layer = [CAShapeLayer layer];
        
        layer.path = [path CGPath];
        layer.strokeColor = [[UIColor whiteColor] CGColor];
        layer.lineWidth = 0;
        layer.fillColor = [[UIColor whiteColor] CGColor];
        [detailView.layer addSublayer:layer];
    }
    else
    {
        if ([[detailView.layer sublayers] count] > 0)
        {
            [[[detailView.layer sublayers] objectAtIndex:0] removeFromSuperlayer];
        }
        UIImageView *resultView = (UIImageView *)[cell viewWithTag:300];
        if([bet[@"payout"] intValue] > 0)
        {
            resultView.image = [UIImage imageNamed:@"Happy Face.gif"];
        }
        else
        {
            resultView.image = [UIImage imageNamed:@"Sad Face.gif"];
        }
    }
    return cell;
}

@end
