//
//  FrontViewController.m
//  socialbookie
//
//  Created by David Hao on 3/9/14.
//  Copyright (c) 2014 David Hao. All rights reserved.
//

#import "FrontViewController.h"
#import "BackendCaller.h"

@interface FrontViewController ()

@end

@implementation FrontViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)buttonPressed:(id)sender {
    NSLog(@"Button pressed, checking login status...");
    [self checkStatus];
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [BackendCaller getStatus];
}
- (void) checkStatus
{
    if([BackendCaller retrieveStatus])
    {
        NSLog(@"Moving from front to feed");
        [self performSegueWithIdentifier:@"FrontToFeed" sender:nil];
    }
    else
    {
        NSLog(@"Moving from front to login");
        [self performSegueWithIdentifier:@"FrontToLogin" sender:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
