//
//  BackendCaller.m
//  socialbookie
//
//  Created by David Hao on 2/10/14.
//  Copyright (c) 2014 David Hao. All rights reserved.
//

#import "BackendCaller.h"
#import "AFHTTPRequestOperationManager.h"

static AFHTTPRequestOperationManager *manager;
static NSDictionary *feedResult;
static NSDictionary *photoResult;
static NSDictionary *userResult;
static NSDictionary *betFeedResult;
static NSDictionary *status;
static NSDictionary *best;
static int money;
static NSDictionary *moneyRanking;
static NSDictionary *popularRanking;

@implementation BackendCaller
+ (void) initialize
{
    manager  =[AFHTTPRequestOperationManager manager];
}

+ (void)getFeed
{
    NSLog(@"Retrieving user feed...");
    [manager GET:@"http://praisal.herokuapp.com/users/feed" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        feedResult = @{@"feed":responseObject};
        NSLog(@"Feed retrieved");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"feedReceived" object:nil];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
+(void) getBetFeed
{
    NSLog(@"Getting bet feed");
    [manager GET:@"http://praisal.herokuapp.com/bets" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        betFeedResult = @{@"feed":responseObject};
        NSLog(@"Bet feed retrieved");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"betFeedReceived" object:nil];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
+(void) getUserDataWithId: (long) userID
{
    NSLog(@"Getting user info for user %ld", userID);
    [manager GET:[NSString stringWithFormat: @"http://socialbookie.herokuapp.com/user_info/%ld", userID] parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
        userResult = (NSDictionary *) responseObject;
        NSLog(@"Info retrieved");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"userDataReceived" object:nil];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error %@", error);
    }];
}
+(NSDictionary *) retrieveUserData
{
    return userResult;
}
+(NSArray *) retrieveBetFeedData
{
    return betFeedResult[@"feed"];
}
+(NSDictionary *) retrieveFeedData
{
    return feedResult;
}

+(void) getURL:(NSString *)url
{
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        status = (NSDictionary *) responseObject;
        NSLog(@"Auth url done");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"url done" object:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

+ (void) getPhotowithPhotoID : (NSString *)photoID andUserID : (long) userID;
{
    [manager GET:[@"http://socialbookie.herokuapp.com/pic/" stringByAppendingString:photoID] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        photoResult = (NSDictionary *) responseObject;
        long testID = [[[[photoResult objectForKey:@"data"] objectForKey:@"user"] valueForKey:@"id"] intValue];
        
        [manager GET:[@"http://socialbookie.herokuapp.com/average/" stringByAppendingFormat:@"%lu", (unsigned long)testID] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            userResult = (NSDictionary *) responseObject;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"userandPhotoReceived" object:nil];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
+ (NSDictionary *) retrievePhotoData
{
    return @{@"user" : userResult, @"photo" : photoResult};
}
+(void) getStatus
{
    NSLog(@"Getting status...");
    [manager GET:@"http://praisal.herokuapp.com/users" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        status = (NSDictionary *) responseObject;
        money = [status[@"money"] intValue];
        NSLog(@"Status retrieved, %d", money);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"statusReceived" object:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
+ (NSDictionary *) retrieveFullStatus
{
    return status;
}
+(BOOL) retrieveStatus
{
    NSLog(@"%d", [[status objectForKey:@"status"] intValue]);
    NSLog(@"%@", status);
    
    if(!status[@"profile_picture"])
    {
        NSLog(@"User is not logged in");
        return NO;
    }
    NSLog(@"User is logged in");
    return YES;
}
+ (void) betOnPic:(NSString *) pic_id WithLikes:(NSNumber*) likes
{
    NSLog(@"Telling api to bet on pic with id: %@", pic_id);
    [manager POST:@"http://praisal.herokuapp.com/bets" parameters:@{@"photo_id":pic_id, @"likes":likes, @"amount":@100 } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *data = (NSDictionary *)responseObject;
        if([data objectForKey:@"message"])
        {
            NSLog(@"%@", data[@"message"]);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"betFailed" object:nil];
        }
        else
        {
            NSLog(@"API says success");
            money = [data[@"money"] intValue];
            NSLog(@"%d", money);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"betConfirmed" object:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

+(int) retrieveMoney
{
    return money;
}

+(NSDictionary *) retrieveBest
{
    return best;
}
+(void) logout
{
    NSLog(@"Logging out...");
    [manager GET:@"http://praisal.herokuapp.com/oauth/logout" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Logged out");
        feedResult = nil;
        photoResult = nil;
        userResult = nil;
        betFeedResult = nil;
        status = nil;
        best = nil;
        NSHTTPCookie *cookie;
        NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        for (cookie in [storage cookies]) {
            [storage deleteCookie:cookie];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loggedOut" object:nil];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

+(void) getPopularRanking
{
    NSLog(@"Getting popular ranking...");
    [manager GET: @"http://praisal.herokuapp.com/users/popular_friends" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        popularRanking = @{@"data":responseObject};
        NSLog(@"Got popular ranking");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"popularRankingReceived" object:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

+(void) getMoneyRanking
{
    NSLog(@"Getting money ranking...");
    [manager GET: @"http://praisal.herokuapp.com/users/friends" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        moneyRanking = @{@"data":responseObject};
        NSLog(@"Money ranking retrieved.");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"moneyRankingReceived" object:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

+(NSArray *) retrievePopularRanking
{
    return popularRanking[@"data"];
}

+(NSArray *) retrieveMoneyRanking
{
    return moneyRanking[@"data"];
}

@end
