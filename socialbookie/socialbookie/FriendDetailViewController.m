//
//  FriendDetailViewController.m
//  socialbookie
//
//  Created by David Hao on 7/11/14.
//  Copyright (c) 2014 David Hao. All rights reserved.
//

#import "FriendDetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "BackendCaller.h"

@interface FriendDetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *profileView;
@property (weak, nonatomic) IBOutlet UILabel *moneyView;
@property (weak, nonatomic) IBOutlet UINavigationItem *titleNavView;
@property (weak, nonatomic) IBOutlet UILabel *averageView;
@property (weak, nonatomic) IBOutlet UICollectionView *bestView;
@property NSArray *best;
@property (weak, nonatomic) IBOutlet UILabel *followerView;

@end

@implementation FriendDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

-(void) viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%@", self.friend);
    [self.bestView setDataSource:self];
    [self.bestView setDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.profileView setImageWithURL:[NSURL URLWithString:self.friend[@"profile_picture"]]];
    if ([self.friend[@"full_name"] isEqualToString:@""]) {
        [self.titleNavView setTitle:self.friend[@"username"]];
    }
    else
    {
        [self.titleNavView setTitle:[NSString stringWithFormat:@"%@ (%@)", self.friend[@"full_name"], self.friend[@"username"]]];
    }
    [self.moneyView setText:[NSString stringWithFormat:@"$%@", self.friend[@"money"]]];
    [self.averageView setText:[NSString stringWithFormat:@"%@%%", self.friend[@"average"]]];
    [self.followerView setText:[NSString stringWithFormat:@"%@", self.friend[@"followers"]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.friend[@"best_pictures"] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *photo = (NSDictionary *)[self.friend[@"best_pictures"] objectAtIndex:indexPath.row];
    static NSString *identifier = @"Cell";
    UICollectionViewCell *cell = [self.bestView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    UIImageView *picture = (UIImageView *)[cell viewWithTag:100];
    [picture setImageWithURL:[NSURL URLWithString:(NSString *)photo[@"large_link"]]];
    return cell;
}
@end
