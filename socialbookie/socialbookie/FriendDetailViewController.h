//
//  FriendDetailViewController.h
//  socialbookie
//
//  Created by David Hao on 7/11/14.
//  Copyright (c) 2014 David Hao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendDetailViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>
    @property NSDictionary *friend;
@end
