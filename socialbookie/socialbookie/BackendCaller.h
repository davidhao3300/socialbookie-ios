//
//  BackendCaller.h
//  socialbookie
//
//  Created by David Hao on 2/10/14.
//  Copyright (c) 2014 David Hao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackendCaller : NSObject
+ (void) getFeed;
+ (NSDictionary *) retrieveFeedData;
+ (void) getPhotowithPhotoID : (NSString *)photoID andUserID : (long) userID;
+ (NSDictionary *) retrievePhotoData;
+ (void) betOnPic:(NSString *) pic_id WithLikes:(NSNumber *) likes;
+(void) getStatus;
+(BOOL) retrieveStatus;
+(NSDictionary *) retrieveFullStatus;
+(void) getUserDataWithId:(long) userID;
+(NSDictionary *) retrieveUserData;
+(void) getBestForUserWithID:(long) userID;
+(void) getBetFeed;
+(NSDictionary *) retrieveBest;
+(void)getURL:(NSString *) url;
+(int)retrieveMoney;
+(void) logout;
+(void) getPopularRanking;
+(void) getMoneyRanking;
+(NSArray *) retrieveBetFeedData;
+(NSArray *) retrievePopularRanking;
+(NSArray *) retrieveMoneyRanking;
@end
