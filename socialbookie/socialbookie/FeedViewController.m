//
//  FeedViewController.m
//  
//
//  Created by David Hao on 2/10/14.
//
//

#import "FeedViewController.h"
#import "BackendCaller.h"
#import "UIImageView+AFNetworking.h"

@interface FeedViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *moneyView;
    @property NSMutableArray *feedPhotos;
@property (weak, nonatomic) IBOutlet UIView *panelView;
@property NSArray *feed;
@property (weak, nonatomic) IBOutlet UILabel *nameField;
@property (weak, nonatomic) IBOutlet UILabel *dataView;
@property UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UILabel *messageView;
@end

@implementation FeedViewController
{
    CAShapeLayer *timerLayer;
    int time;
    NSInteger currentIndex;
    int betted;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

-(void) updateMoney
{
    self.moneyView.text = [NSString stringWithFormat:@" You have: $%d", [BackendCaller retrieveMoney]];
}

- (IBAction)logoutPressed:(id)sender {
    NSLog(@"WTF");
    [BackendCaller logout];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initializeObservers];
    self.moneyView.alpha = 0;
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    if(!self.feed)
    {
        [self startRefresh];
        [BackendCaller getStatus];
    }
    UIImage *headerImage = [UIImage imageNamed:@"logo.gif"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:headerImage];
    self.navigationItem.titleView = imageView;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.clipsToBounds = YES;
    CGRect frame = imageView.frame;
    frame.size.height = 40;
    frame.size.width = 200;
    imageView.frame = frame;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.messageView.layer.zPosition = 20;
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startRefresh)
             forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
}
-(void) startRefresh
{
    [BackendCaller getFeed];
}

-(void) redisplay
{
    NSLog(@"Starting redisplay");
    self.feed = [[BackendCaller retrieveFeedData] valueForKey:@"feed"];
    self.feedPhotos = [[NSMutableArray alloc] init];
    for (int i = 0; i < [self.feed count]; i++)
    {
        NSDictionary *photo = [self.feed objectAtIndex:i];
        [self.feedPhotos addObject:photo];
    }
    [self updateMoney];
    self.moneyView.alpha = 1;
    self.moneyView.layer.zPosition = 100;
    
    
    [self.collectionView reloadData];
    currentIndex = 0;
    [self moving];
    [self.refreshControl endRefreshing];
}

-(void) outOfMoney
{
    NSLog(@"Out of money.");
    [self.messageView setText:@"You don't have enough money!"];
    [UIView animateWithDuration:2.0
                     animations:^{
                         self.messageView.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:2.0
                                               delay:2.0
                                             options:0
                                          animations:^{
                                              self.messageView.alpha = 0;
                                          }
                                          completion:nil];
                     }
     ];
}

-(void) initializeObservers
{
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(updateMoney) name:@"statusReceived" object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(outOfMoney) name:@"outOfMoney" object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(redisplay) name:@"feedReceived" object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(betConfirmed) name:@"betConfirmed" object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(betFailed) name:@"betFailed" object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(logout) name:@"loggedOut" object:nil];
}

-(void) logout
{
    [self performSegueWithIdentifier:@"logout" sender:nil];
}

-(void) betConfirmed
{
    NSLog(@"Confirmed");
    [self.messageView setText:@"The Wait Begins..."];
    [self updateMoney];
    [self.feedPhotos removeObjectAtIndex:betted];
    [self.collectionView reloadData];
    [UIView animateWithDuration:2.0
        animations:^{
            self.messageView.alpha = 1;
        }
        completion:^(BOOL finished){
            [UIView animateWithDuration:2.0
                delay:2.0
                options:0
                animations:^{
                    self.messageView.alpha = 0;
                }
                completion:nil];
        }
     ];
}

-(void) betFailed
{
    NSLog(@"Failed");
    [self.messageView setText:@"You already bet on this picture!"];
    [UIView animateWithDuration:2.0
                     animations:^{
                         self.messageView.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:2.0
                                               delay:2.0
                                             options:0
                                          animations:^{
                                              self.messageView.alpha = 0;
                                          }
                                          completion:nil];
                     }
     ];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.feedPhotos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@", self.feedPhotos);
    NSDictionary *photo = [self.feedPhotos objectAtIndex:indexPath.row];
    //NSLog(@"%@", photo);
    static NSString *identifier = @"Cell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    UIImageView *photoImageView = (UIImageView *)[cell viewWithTag:100];
    NSURL *url = [NSURL URLWithString: (NSString *)(photo[@"images"][@"standard_resolution"][@"url"])];
    [photoImageView setImageWithURL:url];
    return cell;
}

- (IBAction)highBet:(UIButton *)sender {
    NSIndexPath *selectedIndexPath = nil;
    CGPoint center = CGPointMake(160, 284);
    selectedIndexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:center fromView:sender.superview.superview]];    NSDictionary *photo = [self.feedPhotos objectAtIndex:selectedIndexPath.row];
    betted = (int)selectedIndexPath.row;
    NSLog(@"Betting high on picture with id: %@", photo[@"id"]);
    [BackendCaller betOnPic: photo[@"id"] WithLikes:@100];
}

- (IBAction)lowBet:(UIButton *)sender {
    NSIndexPath *selectedIndexPath = nil;
    CGPoint center = CGPointMake(160, 284);
    selectedIndexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:center fromView:sender.superview.superview]];
    NSDictionary *photo = [self.feedPhotos objectAtIndex:selectedIndexPath.row];
    betted = (int)selectedIndexPath.row;
    
    NSLog(@"Betting low on picture with id: %@", photo[@"id"]);
    [BackendCaller betOnPic: photo[@"id"] WithLikes:@0];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    NSLog(@"%@", segue.identifier);
}


-(void) viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:@"feedReceived" object:nil];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:@"betFailed" object:nil];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:@"betConfirmed" object:nil];
    [[NSNotificationCenter defaultCenter]
         removeObserver:self name:@"statusReceived" object:nil];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:@"loggedOut" object:nil];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [UIView animateWithDuration:0.25 animations:^{self.moneyView.alpha=0;}];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGPoint center = CGPointMake(160, 200);
    NSIndexPath *selectedIndexPath = [self.collectionView indexPathForItemAtPoint:[self.collectionView convertPoint:center fromView:self.collectionView.superview]];
    
    if (currentIndex != selectedIndexPath.row)
    {
        currentIndex = selectedIndexPath.row;
        [self moving];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [UIView animateWithDuration:0.5 animations:^{self.moneyView.alpha=1;}];
}

-(void) moving
{
    if ([self.feed count] == 0) return;
    NSDictionary *photo = (NSDictionary *)[self.feed objectAtIndex:currentIndex];
    int newTime = [photo[@"created_time"] intValue];
    time = newTime;
    [self drawTimer];
    
    if (photo[@"user"][@"full_name"])
    {
        [self.nameField setText:photo[@"user"][@"full_name"]];
    }
    else
    {
        [self.nameField setText:photo[@"user"][@"username"]];
    }
    
    int average = [photo[@"likes"][@"count"] doubleValue] / [photo[@"average"][@"followers"] doubleValue];
    [self.dataView setText:[NSString stringWithFormat:@"%d%% -> %@%%?", average, photo[@"average"][@"percentage"]]];
    
    if(average < 0)
    {
        [self.dataView setText: @"Loading..."];
    }
}

-(void) drawTimer
{
    long currentTime = [[NSDate date] timeIntervalSince1970];
    float diff = 1.0*(currentTime - time) / 60/60;
    float pi = 3.1415;
    float endAngle = -0.5*pi;
    float startAngle = -1*diff*pi*2/5 + 1.5*pi;
    
    NSLog(@"called, %f, %f, %f", diff, startAngle, endAngle);
    
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(160, 43) radius:20 startAngle:startAngle endAngle:endAngle clockwise:false];
    [path addLineToPoint:CGPointMake(160, 43)];
    
    if(timerLayer)
    {
        [timerLayer removeFromSuperlayer];
    }
    
    timerLayer = [CAShapeLayer layer];
    timerLayer.path = [path CGPath];
    timerLayer.strokeColor = [[UIColor whiteColor] CGColor];
    timerLayer.lineWidth = 0;
    timerLayer.fillColor = [[UIColor whiteColor] CGColor];
    
    [self.panelView.layer addSublayer:timerLayer];
}

@end
