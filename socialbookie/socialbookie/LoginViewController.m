//
//  LoginViewController.m
//  socialbookie
//
//  Created by David Hao on 2/10/14.
//  Copyright (c) 2014 David Hao. All rights reserved.
//

#import "LoginViewController.h"
#import "BackendCaller.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *outhView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation LoginViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void) checkLogin
{
    if([BackendCaller retrieveStatus])
    {
        [self.activityIndicator stopAnimating];
        NSLog(@"User has successfully logged in");
        NSLog(@"Moving from Login to Feed");
        [self performSegueWithIdentifier:@"LoginToFeed" sender:nil];
    }
}
- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *url = [request URL];
    NSString *urlString = [NSString stringWithFormat: @"%@", url];
    //NSLog(@"%@", urlString);
    if([urlString hasPrefix:@"http://praisal.herokuapp.com/oauth/callback?"])
    {
        NSLog(@"%@", urlString);
        [BackendCaller getURL:urlString];
        return NO;
    }
    return YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(checkLogin) name:@"url done" object:nil];
    [self.outhView setDelegate:self];
    NSString *url = @"http://praisal.herokuapp.com/oauth";
    NSURL *authURL = [NSURL URLWithString:url];
    NSURLRequest *authRequest = [NSURLRequest requestWithURL:authURL];
    NSLog(@"Accessing login website...");
    [self.outhView loadRequest:authRequest];
    NSLog(@"Auth page displayed");
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:@"url done" object:nil];
}

@end
