//
//  FriendFeedViewController.m
//  socialbookie
//
//  Created by David Hao on 7/10/14.
//  Copyright (c) 2014 David Hao. All rights reserved.
//

#import "FriendFeedViewController.h"
#import "BackendCaller.h"
#import "UIImageView+AFNetworking.h"
#import "FriendDetailViewController.h"

@interface FriendFeedViewController ()
    @property NSArray *feed;
    @property UIRefreshControl *refreshControl;
@end

@implementation FriendFeedViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startRefresh)
                  forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
}

-(void) startRefresh
{
    NSLog(@"Starting refresh");
    [BackendCaller getMoneyRanking];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(refresh) name:@"moneyRankingReceived" object:nil];
    [self startRefresh];
}

-(void) refresh
{
    self.feed = [BackendCaller retrieveMoneyRanking];
    
    NSLog(@"Refresh completed");
    NSLog(@"%@", self.feed);
    [self.collectionView reloadData];
    [self.refreshControl endRefreshing];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.feed count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *friend = [self.feed objectAtIndex:indexPath.row];
    static NSString *identifier = @"Cell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *photoImageView = (UIImageView *)[cell viewWithTag:100];
    NSURL *url = [NSURL URLWithString: (NSString *)(friend[@"profile_picture"])];
    [photoImageView setImageWithURL:url];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *friend = [self.feed objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"friendFeedToDetail" sender:friend];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"friendFeedToDetail"])
    {
        FriendDetailViewController *controller = segue.destinationViewController;
        controller.friend = sender;
    }
}

@end
