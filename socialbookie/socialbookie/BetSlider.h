//
//  BetSlider.h
//  socialbookie
//
//  Created by David Hao on 2/20/14.
//  Copyright (c) 2014 David Hao. All rights reserved.
//
#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>

@class BetSliderValuePopupView;

@interface BetSlider : UISlider {
    BetSliderValuePopupView *valuePopupView;
    
}
-(void)_fadePopupViewInAndOut:(BOOL)aFadeIn;
@property (nonatomic, readonly) CGRect thumbRect;

@end

